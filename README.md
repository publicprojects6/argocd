# Projeto Gitops ArgoCD

### User: admin
### Pegar Password: 

```
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
```

### Pasta "no-ha": Configurações do ArgoCD. ---> status: Pronto
### Pasta "root": Configurações do Argo CD ao seu próprio repositório e projeto para gestão das configurações do Argo CD no cluster Kubernetes. ---> Pronto
### Pasta "structure": Configurações dos projetos, apps e seus repositórios, respctivamente. ---> status: Pronto

# Instruções para usar o recusos do ArgoCD no cluster Kubernetes

# Para utilizar o ArgoCD pela primeira vez, é necessário fazer a implementação do mesmo no Cluster Kubernetes de forma manual. Este processo precisa ser efetuado apenas uma vez, e neste caso o Terraform faz a execução.

# * Este projeto também pode ser utilizado sem o Terraform. Para isto execute os comandos abaixo:

### Certifique-se de estar autenticado no cluster Kubernetes no qual vai aplicar a configuração:

## Instalar o ArgoCD no Cluster

### 1 - Clone o projeto:

```
git clone https://gitlab.com/publicprojects6/argocd.git
```

### 2 - Navegue para o diretório root do projeto clonado no passo anterior:

```
cd argocd
```

### 3 - Crie o namespace do ArgoCD:
 
```
kubectl apply -f argocd/no-ha/base/namespace.yaml -n argocd
```

### 4 - Instale o ArgoCD no cluster Kubernetes:

```
kubectl apply -f argocd/no-ha/base/install.yaml -n argocd
```

### 5 - Crie a secret do repositorio do ArgoCD

```
kubectl apply -f argocd/root/base/01-repositories.yaml -n argocd
```

### 6 - Crie o projeto do ArgoCD

```
kubectl apply -f argocd/root/base/02-projects.yaml -n argocd
```

### 7 - Crie as aplicações do ArgoCD conforme o ambiente (podem ser criados quantos ambientes forem necessários)

```
kubectl apply -f argocd/root/overlays/<stg/prd>/03-apps.yaml -n argocd
```

### 8 - Recrie a secret dos repositórios das apps de features e security

```
kubectl apply -f argocd/structure/repositories/base/argocd.yaml -n argocd
```

### 9 - Recrie a secret dos repositórios das apps proprietárias

```
kubectl apply -f argocd/structure/repositories/base/desafio-devops.yaml -n argocd
```

### À partir deste momento o ArgoCD passa a gerenciar a aplicação de todas os deploys de aplicações proprietárias, feature, secrets e demais manifestos. Ou seja, toda a gestão fica a cargo de configurações em repositórios e o ArgoCD detecta as mudanças e as aplica.

# Gitflow

### Todas as alterações devem ser feitas via branchs oriundas de issues e copiadas da master. Na criação destas branches deve-se utilizar o padrão de feature/*. Após a alteração ser concluída deve-se fazer um merge com a master via Merge Request e solicitar a aprovação dos manteiners.

### Após a aplicação das alterações estarem na master poderá o mesmo fazer o merge para a branch "stg" e depois de validado o funcionamento em "stg" poderá fazer o merge request da branch de "stg" para "prd".

### Segue representação gráfica abaixo:

![alt text for screen readers](./image/gitlab-flow.png )